﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        private readonly string SEPARATOR = "\n--------------------------------------------------------------------\n";
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        //TODO: Добавить методы получения данных для банкомата

        public void ShowUserInfo(string login, string password)
        {
            Console.WriteLine(Users.Single(u => u.Login == login && u.Password == password));
        }

        public void ShowAccountInfos(string login, string password)
        {
            var accounts = Users.Where(u => u.Login == login && u.Password == password)
                .GroupJoin(Accounts,
                    u => u.Id,
                    a => a.UserId,
                    (user, accs) => new {User = user, List = accs}).Single();

            Console.WriteLine($"User accounts:{SEPARATOR}{accounts.User}\n\n{String.Join("\n", accounts.List)}{SEPARATOR}");
        }

        public void ShowExtendedAccountInfos(string login, string password)
        {
            Console.WriteLine("Extended account infos:" + SEPARATOR);
            
            var userAccounts = Users.Where(u => u.Login == login && u.Password == password)
                .GroupJoin(Accounts,
                    u => u.Id,
                    a => a.UserId,
                    (user, accs) => accs).Single();
            var extendedAccInfos = History.GroupBy(h => h.AccountId).Join(userAccounts,
                group => group.Key,
                acc => acc.Id,
                (group, acc) => new
                {
                    Account = acc.ToString(),
                    History = String.Join("\n", group.ToList())
                }).ToList();

            extendedAccInfos.ForEach(info => Console.WriteLine($"{info.Account}\n\n{info.History}\n"));
            Console.WriteLine(SEPARATOR);
        }

        public void ShowAccountsIncomes()
        {
            Console.WriteLine("Accounts income history:" + SEPARATOR);

            var incomes = History.Where(h => h.OperationType == OperationType.InputCash)
                .GroupBy(h => h.AccountId)
                .Join(Accounts,
                    group => group.Key,
                    account => account.Id,
                    (group, acc) => new
                    {
                        Account = acc,
                        IncomeHistory = group.ToList()
                    }).Join(Users,
                    accHistory => accHistory.Account.UserId,
                    u => u.Id,
                    (accHistory, user) => new
                    {
                        User = user,
                        Account = accHistory.Account,
                        IncomeHistory = accHistory.IncomeHistory
                    }).ToList();

            incomes.ForEach(inc => Console.WriteLine($"{inc.Account}\n{inc.User}\nIncomes:\n{String.Join("\n", inc.IncomeHistory)}\n\n"));

            Console.WriteLine(SEPARATOR);
        }

        public void ShowUsersWithTotalCashAmongAllAccountsHigherThan(decimal amount)
        {
            Console.WriteLine($"Users with total cash among all accounts higher than {amount}:{SEPARATOR}");

            var result = Accounts.GroupBy(acc => acc.UserId)
                .Where(group => group.Sum(acc => acc.CashAll) > amount)
                .Join(Users,
                    group => group.Key,
                    user => user.Id,
                    (g, u) => u).ToList();

            if (result.Any())
            {
                Console.WriteLine(String.Join("\n\n", result) + SEPARATOR);
            }
            else
            {
                Console.WriteLine("No users found" + SEPARATOR);
            }
        }

        public void ShowUsersWithTotalCashOnOneAccountHigherThan(decimal amount)
        {
            Console.WriteLine($"Users with total cash on one account higher than {amount}:{SEPARATOR}");

            var result = Accounts.Where(acc => acc.CashAll > amount)
                .GroupBy(acc => acc.UserId)
                .Join(Users,
                    group => group.Key,
                    user => user.Id,
                    (g, u) => u).ToList();

            if (result.Any())
            {
                Console.WriteLine(String.Join("\n\n", result) + SEPARATOR);
            }
            else
            {
                Console.WriteLine("No users found" + SEPARATOR);
            }
        }

    }
}